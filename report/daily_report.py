# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.report import Report
from trytond.pool import Pool

import string
from datetime import datetime, date, timedelta, time

__all__ = ['ReportAppointmentDaily']

class ReportAppointmentDaily(Report):
    __name__ = 'gnuhealth.report.daily'
   
    @classmethod
    def get_context(cls, records, data):
        context = super(ReportAppointmentDaily, cls).get_context(records, data)
        appointments = None
        pool = Pool()
        if 'appointment_date' in data:
            Appointments = pool.get('gnuhealth.appointment')
            start_date = datetime.combine(data['appointment_date'],time())
            end_date = datetime.combine(data['appointment_date'],time(hour=23,minute=59))
            appointments = Appointments.search([
                ('appointment_date','>=',start_date),
                ('appointment_date','<=',end_date),
                ('healthprof','in',data['health_profs']),
                ('patient','>',0),
                ])
            context['appointment_date'] = data['appointment_date']
            context['objects'] = appointments
        else:            
            Date = pool.get('ir.date')
            appointments = records
            context['appointment_date'] = Date.today()        
        context['activities'] = set()
        context['health_profs'] = set()
        context['health_prof_name'] = {}
        context['health_prof_lastname'] = {}
        context['appointment_dates'] = set()
        for appt in appointments:
            if appt.healthprof:
                context['health_profs'].add(appt.healthprof.id)
                context['health_prof_name'][str(appt.healthprof.id)] =\
                    appt.healthprof.name.name
                context['health_prof_lastname'][str(appt.healthprof.id)] =\
                    appt.healthprof.name.lastname
            if appt.activity_type == 'individual':                
                context['activities'].add(appt.individual_activity)                    
            elif appt.activity_type == 'groupal':                
                context['activities'].add(appt.groupal_activity)
            elif appt.activity_type == 'institutional':                
                context['activities'].add(appt.institutional_activity)
            elif appt.activity_type == 'community':                
                context['activities'].add(appt.community_activity)
            if appt.appointment_date:
                context['appointment_dates'].add((appt.appointment_date\
                    +timedelta(hours=-3)).date())
        return context
