#! -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health import *
from .report import *
from .wizard import *

def register():
    Pool.register(
        PartyPatient,
        Appointment,
        HealthProfessional,
        CreateAppointmentStart,
        PrintDailyAppointmentStart,        
        module='health_daily_report_fiuner', type_ = 'model' )
    Pool.register(        
        CreateAppointment,
        PrintDailyAppointment,
        module='health_daily_report_fiuner', type_ = 'wizard')
    Pool.register(
        ReportAppointmentDaily,
        module='health_daily_report_fiuner', type_ = 'report')
    
