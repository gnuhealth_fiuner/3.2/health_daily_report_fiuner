#! -*- coding: utf-8 -*-
#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval

__all__ = ['MantenimientoSequences','Configuration']


class MantenimientoSequences(ModelSingleton, ModelSQL, ModelView):
    'Standard Sequences for Mantenimiento'
    __name__ = 'mantenimiento.sequences'
    
    historial_sequence = fields.Property(fields.Many2One(
        'ir.sequence', 'Historial Sequence', required=True,
        domain=[('code', '=', 'mantenimiento.historial')]))
    
class Configuration(ModelSingleton, ModelView, ModelSQL):
    'Accounts configuration for templates'
    __name__ = 'mantenimiento.configuration'
    
    depreciation_duration_default = fields.Property(fields.Integer(u'Período de amortización\npor defecto, en meses',
                                                                   required=True))
    
    list_price_default = fields.Property(fields.Numeric(u'Precio de venta',
                                                required=True))
    
    cost_price_default = fields.Property(fields.Numeric(u'Precio de coste',
                                                required=True))
    
    #account_depreciation_default = fields.Property(fields.Many2One('account.account',u'Cuenta de amortización',
                                                                   #required=True))
    
    #account_asset_default = fields.Property(fields.Many2One('account.account',u'Cuenta de activo',
                                                    #required=True))
    