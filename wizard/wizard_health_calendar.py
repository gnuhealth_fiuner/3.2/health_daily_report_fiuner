# -*- coding: utf-8 -*-
##############################################################################
#

#
##############################################################################
from datetime import timedelta, datetime, time
import pytz
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['CreateAppointmentStart', 'CreateAppointment']


class CreateAppointmentStart(metaclass=PoolMeta):
    'Create Appointments Start'
    __name__ = 'gnuhealth.calendar.create.appointment.start'

    daily_appointment_quantity = fields.Integer(u'Cantidad de turnos por día', 
                                                help="Cantidad de turnos a otorgar")   
    

    @fields.depends('appointment_minutes', 'time_end', 'time_start',
                    'daily_appointment_quantity')
    def on_change_with_daily_appointment_quantity(self):
        # Return the quantity of appointment per day
        if self.appointment_minutes and self.time_end and self.time_start and not self.daily_appointment_quantity:
            delta_hours = self.time_end.hour - self.time_start.hour
            delta_minutes = self.time_end.minute - self.time_start.minute
            delta_time = (delta_hours*60+delta_minutes) if delta_hours>0 else 0
            appointment_quantity = int((delta_time)/self.appointment_minutes)
            return appointment_quantity
        return self.daily_appointment_quantity
   
    @fields.depends('daily_appointment_quantity', 'time_end', 'time_start',
                    'appointment_minutes')
    def on_change_with_appointment_minutes(self):
        # Return the quantity of appointment per day
        if self.daily_appointment_quantity and self.time_end and self.time_start and not self.appointment_minutes:
            delta_hours = self.time_end.hour - self.time_start.hour
            delta_minutes = self.time_end.minute - self.time_start.minute
            delta_time = (delta_hours*60+delta_minutes) if delta_hours>0 else 0
            appointment_minutes = int((delta_time)/self.daily_appointment_quantity)
            return appointment_minutes
        return self.appointment_minutes
    
    @fields.depends('healthprof')
    def on_change_date_start(self):
        if self.healthprof:
            self.monday = self.healthprof.monday
            self.tuesday = self.healthprof.tuesday
            self.wednesday = self.healthprof.wednesday
            self.thursday = self.healthprof.thursday
            self.friday = self.healthprof.friday
            self.saturday = self.healthprof.saturday
            self.sunday= self.healthprof.sunday
            self.time_start = self.healthprof.time_start
            self.time_end = self.healthprof.time_end
            self.appointment_minutes = self.healthprof.appointment_minutes
            self.daily_appointment_quantity = self.healthprof.daily_appointment_quantity


class CreateAppointment(metaclass=PoolMeta):
    'Create Appointment'
    __name__ = 'gnuhealth.calendar.create.appointment'    
    
    
